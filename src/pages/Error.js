import Banner from '../components/Banner';

// import {Row, Col} from 'react-bootstrap';

// import {Link} from 'react-router-dom';

export default function Error() {

    const data = {
        title: "Error 404 - Page not found.",
        content: "The page you are looking for cannot be found.",
        destination: "/",
        label: "Back to Home"
    }

    return (

        <Banner data={data} />

     //    <Row>
     //    	<Col className="p-5 text-center">
     //            <h1>Page Not Found</h1>
     //            <p>Go back to the <Link style={{textDecoration: 'none'}} to="/">homepage</Link>.</p>
                
     //        </Col>
     //    </Row>
    )
};
import coursesData from '../data/CoursesData';
import CourseCard from '../components/CourseCard';
import {useState, useEffect} from 'react';

export default function Courses() {

	// Checks to see if the mock data was captured
	// console.log(coursesData);
	// console.log(coursesData[0]);

	// State that will be used to store the courses retrieved from the database
	const [courses, setCourses] = useState([]);

	// everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our coursesData array using the "courseProp"
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} course={course} />
	// 	)
	// })

	// Retrieves the courses from the database upon initial render of "courses" component
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course => {
				return (
					<CourseCard key={course._id} course={course} />
				)
			}))
		})
	  // dependency	
	}, []);

	return(
		<>
		{courses}
		</>
	)
}
import React from 'react';

// "React.createContext" - allows us to pass information between components without using props drilling
const UserContext = React.createContext();

// "Provider" = allows other components to consume/use the context object and supply necessary information needed for the context object
export const UserProvider = UserContext.Provider;

export default UserContext;